---
title: "Về chúng tôi"
meta_title: "Về chúng tôi"
description: "Về chúng tôi"
---

Trung tâm văn hóa Phật giáo Việt Nam bang Sachsen được thành lập ngày 13 tháng 3 năm 2011 tại thành phố Dresden với sự chứng minh của Hội đồng cố vấn, Đại sứ quán Việt Nam cùng rất đông đảo bà con cộng đồng người Việt bang Sachsen. Thiền viện Khánh An-CHLB Đức được ra đời ngày 03.09.2023 nhờ sự hướng tâm cao cả của thầy Bổn sư-Thượng tọa Thích Trí Chơn cùng quí chư tăng ni cũng như sự phát tâm mạnh mẽ của quí phật tử và bà con kính mến Đạo Phật thuộc Trung tâm văn hóa Phật giáo Việt Nam bang Sachsen tại CHLB Đức và quốc tế.

Sứ mệnh trọng đại của Thiền viện Khánh An là phát huy tư tưởng giáo lý trong sáng, mầu nhiệm của Phật giáo và bảo tồn các giá trị văn hóa của Đạo Phật và dân tộc Việt Nam. Sự ra đời của Thiền viện Khánh An tại bang Sachsen được xem như là nét son mới trong trang sử của cộng đồng Phật tử và những người yêu kính Đạo Phật trên quê hương thứ hai này. 

Chúng tôi tha thiết kêu gọi tất cả chúng ta hãy cùng nhau chung sức chung lòng, xây dựng một ngôi nhà chung làm điểm tựa tâm linh, hầu đem lại niềm an lạc, hạnh phúc và một cuộc sống văn hóa truyền thống thanh cao cho bản thân ta, gia đình ta và cho cộng đồng xã hội. Chúng tôi tin tưởng với đạo tâm dũng mãnh và sự chuyên cần tu tập theo chánh pháp, chúng ta sẽ hoàn thành ước nguyện mà bao nhiêu đồng hương đang mong đơi .

Nguyện cầu Tam Bảo gia hộ cho chúng ta đạo tâm kiên cố, đạo hạnh trang nghiêm vững bước trên con đường phụng sự Chính pháp, bảo tồn và phát triển mãi nét đẹp thanh cao của nền văn hóa Việt phục vụ cộng đồng.

Nam mô hoan hỷ Tạng Bồ tát Ma Ha Tát!!!

#### Liên hệ - Địa chỉ
Thiền Viện Khánh An

Đường Tal số 33

01816 Bad Gottleuba - Berggießhübel

Điện thoại: +49 35023 573999

#### Thông tin ngân hàng
Chủ tài khoản: ViBuz. tại Sachsen

IBAN: DE 16 8505 0300 0221 2685 53

Ngân hàng: Ostsächsische Sparkasse Dresden
