---
title: "Über uns"
meta_title: "Über uns"
description: "Über uns"
---

Das Vietnamesische Buddhistische Kulturzentrum in Sachsen Institut Khanh An für
Mediation e.V. wurde am 13. März 2011 in Dresden gegründet – bezeugt von dem
buddhistisen Beratungsrat, der vietnamesische Botschaft und einer großen Zahl
von Mitgliedern der vietnamesischen Gemeinschaft in Sachsen. Das Institut Khánh
An für Mediation in der Bundesrepublik Deutschland wurde am 03.09.2023 ins
Leben gerufen, dank der erhabenen Hinwendung von dem Erwürdigen Thích Trí Chơn
sowie den Mönchen und Nonnen und dem starken Engagement der buddhistischen
Gläubigen und den den Buddhismus liebenden Bürgerinen und Bürger in der
Gemeinschaft des Vietnamesischen Buddhistischen Kulturzentrums in Sachsen,
sowie in Deutschland und in der Welt. Die bedeutende Mission des
Instituts Khánh An für Mediation besteht darin, die reine,
wundersame Lehre des Buddhismus zu fördern und die kulturellen
Werte des Buddhismus sowie der vietnamesischen Nation zu bewahren.
Die Gründung des Instituts Khánh An für Mediation in Sachsen wird
als einen neuen Höhepunkt in der Geschichte der Buddhisten und der
den Buddhismus liebenden Menschen in dieser „zweiten Heimat“
angesehen.

Wir rufen alle herzlich dazu auf, gemeinsam mit vereinten Kräften ein
gemeinsames Haus als spirituelle Stütze zu errichten, das uns inneren Frieden,
Glück und ein edles, traditionelles kulturelles Leben für uns, unsere Familien
und die Gesellschaft bringt. Wir sind überzeugt, dass wir mit einem mutigen
buddhistischen Geist und dem gewissenhaften Praktizieren der wahren Lehre den
lang ersehnten Wunsch all unserer Landsleute erfüllen werden. Mögen die Drei
Juwelen uns beistehen, damit unser buddhistischer Geist fest, unser ethischer
Charakter würdevoll und unser Weg im Dienst an der wahren Lehre sicher bleibt –
und damit die edle Schönheit der vietnamesischen Kultur zum Wohle der
Gemeinschaft bewahrt und weiterentwickelt wird.

Zur Ehrung Buddha Shakiamuni

#### Kontakt - Anschrift
Institut KHANH AN für Mediation

Talstraße 33

01816 Bad Gottleuba - Berggießhübel

Tel: +49 35023 573999

#### Bankverbindung
Kontoinhaber: ViBuz. in Sachsen

IBAN: DE 16 8505 0300 0221 2685 53

Bank: Ostsächsische Sparkasse Dresden
