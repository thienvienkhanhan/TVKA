---
title: Hoạt động Phật giáo 2025 – Chương trình cả năm cho chánh niệm, sức khỏe
  và cộng đồng
meta_title: Hoạt động Phật giáo 2025 – Chương trình cả năm cho chánh niệm, sức
  khỏe và cộng đồng
description: Hoạt động Phật giáo 2025 – Chương trình cả năm cho chánh niệm, sức
  khỏe và cộng đồng
---
• **Tháng Tư 2025**  

 ◦ 04.04, 17:00 – 05.04, 17:00 – Khóa học nấu ăn thuần chay: “Văn hóa ẩm thực của các tăng ni Việt Nam; Lịch trình trong ngày của một vị tăng”.  

  – Người phụ trách: Anh Đỗ Xuân Ngoc và Chị Bich Nga, đầu bếp có kinh nghiệm (Phật Tử)  

  – Ghi chú: Cần đăng ký trước  

 ◦ 11.04, 17:00 – 13.04, 17:00 – Giới thiệu ngắn gọn về Phật giáo Việt Nam  

  – Người phụ trách: Ông Heiner Dinglinger (Phật Tử)  

  – Ghi chú: Cần đăng ký trước  

 ◦ 18.04, 17:00 – 21.04, 17:00 – Khóa tu “Cho một gia đình hạnh phúc”  

  – Người phụ trách: Thầy Pháp Cứ  

  – Ghi chú: Cần đăng ký trước

• **Tháng Năm 2025**  

 ◦ 02.05, 17:00 – 04.05, 17:00 – Khóa thiền cho mọi người  

  – Người phụ trách: Thầy Pháp Cứ  

  – Ghi chú: Cần đăng ký trước  

 ◦ 09.05, 17:00 – 11.05, 17:00 – Khóa học YOGA  

  – Người phụ trách: Giáo viên …  

  – Ghi chú: Cần đăng ký trước  

 ◦ 17.05, 9:00 – 18.05, 17:00 – Khóa thiền mừng Đại Lễ VESAKH 2025 (10 Tăng Ni từ Làng Mai, Pháp)  

  – Ghi chú: Cần đăng ký trước (đối với lễ VESAKH thì không cần đăng ký)  

 ◦ 30.05, 17:00 – 01.06, 17:00 – Khóa thiền hành cho người lớn và trẻ em  

  – Người phụ trách: Thầy Pháp Chương  

  – Ghi chú: Cần đăng ký trước

• **Tháng Sáu 2025**  

 ◦ 07.06, 9:00 –  09.06, 17:00 – Giới thiệu về môn TAI CHI với mục đích nâng cao sức khỏe  

  – Người phụ trách: Thầy Pháp Cứ  

  – Ghi chú: Cần đăng ký trước  

 ◦ 13.06, 14:00 – 17.06, 14:00 – Khóa học năm ngày về thanh lọc thân tâm  

  – Người phụ trách: Tiến sĩ Bác Sỹ Tâm Thành; Các Phật tử: Chị Luong, Chị Nguyệt và Chị Bich  

  – Ghi chú: Cần đăng ký trước  

 ◦ 27.06, 17:00 – 29.06, 17:00 – Giới thiệu về THAI‑MASSAGE cho việc chăm sóc sức khỏe các khớp và cơ thể  

  – Người phụ trách: Chị Hoa và Chị Ha, hai nhà trị liệu có kinh nghiệm  

  – Ghi chú: Cần đăng ký trước

• **Tháng Bảy 2025**  

 ◦ 18.07, 17:00 – Khóa học nấu ăn thuần chay: “Văn hóa ẩm thực của các tăng ni Việt Nam; Lịch trình trong ngày của một vị tăng”  

  – Người phụ trách: Ạnh Đỗ Xuân Ngoc và Chị Bich Nga, đầu bếp có kinh nghiệm (Phật Tử)  

  – Ghi chú: Cần đăng ký trước  

 ◦ 25.07, 17:00 – 27.07, 17:00 – Khóa học thiền hành cho mọi lứa tuổi  

  – Người phụ trách: Giáo viên và Thầy Pháp Cứ; Phật Tử Đào Ngoc Hanh  

  – Ghi chú: Cần đăng ký trước

• **Tháng Tám 2025**  

 ◦ 03.08, 9:00 – 07.08, 17:00 – Trại hè cho thanh thiếu niên  

  – Người phụ trách: Thầy Pháp Cứ cùng Ban quản trị  

  – Ghi chú: Nếu nghỉ có qua đêm – cần đăng ký trước  

 ◦ 17.08, 10:00 –23.08, 15:00 – Sắp xếp chỗ ăn nghỉ và phục vụ khách cho hội nghị tại Dresden  

  – Người phụ trách: Kerstin & Wilfried (Ban quản trị)  

  – Ghi chú: Đã đăng ký  

 ◦ 29.08, 17:00 – 31.08, 17:00 – Chăm sóc và chữa lành bệnh theo y học cổ truyền phương Đông hay TCM (châm cứu / bấm huyệt)  

  – Người phụ trách: Chị Minh Nguyệt, Kỹ sư Sư Phạm KT, chuyên gia TCM  

  – Ghi chú: Cần đăng ký trước

• **Tháng Chín 2025**  

 ◦ 07.09, 9:00 và 18:00 – Lễ Vu lan báo hiếu (Ullambana): Ngày mở cửa tại Viện KHANH AN  

  – Người phụ trách: Ban quản trị  

  – Ghi chú: Miễn phí vào cửa, không cần đăng ký, số lượng tham dự không giới hạn  

 ◦ 12.09, 17:00 –14.09, 17:00 – Khóa học JOGA nhằm tăng cường sức khỏe  

  – Người phụ trách: Thầy…  

  – Ghi chú: Cần đăng ký trước  

 ◦ 19.09, 17:00 – 21.09, 17:00 – Thực hành thiền đi bộ cho mọi người  

  – Người phụ trách: Thầy Pháp Cứ  

  – Ghi chú: Cần đăng ký trước  

 ◦ 28.09, 14:00 – Lễ hội làng tại ZWIESEL  

  – Người phụ trách: Đại diện làng và Ban quản trị  

  – Ghi chú: Miễn phí vào cửa, số lượng tham dự không giới hạn

• **Tháng Mười 2025**  

 ◦ 03.10, 17:00 – 05.10, 17:00 – Thực hành hai ngày nhân dịp lễ Trung thu  – Khóa tu dành cho gia đình”  

  – Người phụ trách: Thầy Pháp Cứ  

  – Ghi chú: Số lượng khách tham dự không giới hạn; nếu nghỉ qua đêm – cần đăng ký trước  

 ◦ 10.10, 17:00 – 12.10, 17:00 – Massage chân: “Một liệu pháp chăm sóc sức khỏe quan trọng”  

  – Người phụ trách: Chị Hoa và Chị Hà, hai nhà trị liệu có kinh nghiệm  

  – Ghi chú: Cần đăng ký trước  

 ◦ 26.10, 9:00–17:00 – NGÀY CHÁNH NIỆM CHO MỌI NGƯỜI  

  – Người phụ trách: Thầy Pháp Chương (từ Thiền Lâm Tự)  

  – Ghi chú: Số lượng khách tham dự không giới hạn

• **Tháng Mười Một 2025**  

 ◦ 31.10, 17:00 – 02.11, 17:00 – Khóa học TAI CHI chuyên sâu (16 tư thế)  

  – Người phụ trách: Thầy Pháp Cứ  

  – Ghi chú: Cần đăng ký trước  

 ◦ 14.11, 17:00 –16.11, 17:00 – Khóa học Shaolin Qi Gong  

  – Người phụ trách: Thầy Pháp Cứ  

  – Ghi chú: Cần đăng ký trước
