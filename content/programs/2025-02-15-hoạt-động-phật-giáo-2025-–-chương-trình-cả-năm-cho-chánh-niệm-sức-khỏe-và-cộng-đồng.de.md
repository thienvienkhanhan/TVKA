---
title: Buddhistische Aktivitäten 2025 – Ein Jahresprogramm für Achtsamkeit,
  Gesundheit und Gemeinschaf
meta_title: Buddhistische Aktivitäten 2025 – Ein Jahresprogramm für Achtsamkeit,
  Gesundheit und Gemeinschaf
description: Buddhistische Aktivitäten 2025 – Ein Jahresprogramm für
  Achtsamkeit, Gesundheit und Gemeinschaf
---
• **April 2025**  

 ◦ 04.04, 17:00 Uhr – 05.04, 17:00 Uhr – Vegan Kochkurs: „Kulinarische Kultur der vietnamesischen Mönche und Nonnen; Tagesablauf eines Mönches“.  

  – Verantwortlich: Herr Do Xuan Ngoc und Frau Bich Nga, erfahrende Koch/Köchin (Laien)  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 11.04, 17:00 Uhr – 13.04, 17:00 Uhr – Kurze Einführung in den vietnamesischen Buddhismus  

  – Verantwortlich: Herr Heiner Dinglinger (Laie, Dipl.)  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 18.04, 17:00 Uhr – 21.04, 17:00 Uhr – Retreat für eine glückliche Familie  

  – Verantwortlich: Lehrer und Mönch Phap Cu  

  – Bemerkung: Voranmeldung notwendig

• **Mai 2025**  

 ◦ 02.05, 17:00 Uhr – 04.05, 17:00 Uhr – Meditationskurs für Alle  

  – Verantwortlich: Lehrer und Mönch Phap Cu  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 09.05, 17:00 Uhr – 11.05, 17:00 Uhr – YOGA-Kurs  

  – Verantwortlich: Lehrer …  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 17.05, 9:00 Uhr – 18.05, 17:00 Uhr – Meditationskurs zum großen VESAKH-Fest 2025 (10 Mönche und Nonnen aus Plum Village, Frankreich)  

  – Bemerkung: Voranmeldung notwendig (zum VESAK-Fest ohne Anmeldung)  

 ◦ 30.05, 17:00 Uhr – 01.06, 17:00 Uhr – Gehmeditationskurs für Erwachsene und Kinder  

  – Verantwortlich: Lehrer und Mönch Phap Chuong  

  – Bemerkung: Voranmeldung notwendig

• **Juni 2025**  

 ◦ 07.06, 9:00 Uhr –09.06, 17:00 Uhr – Einführung in die Sportart TAI CHI zur Gesundheitsförderung  

  – Verantwortlich: Lehrer und Mönch Phap Cu  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 13.06, 14:00 Uhr –17.06, 14:00 Uhr – Fünftägiger Kurs zur Reinigung von Körper und Seele  

  – Verantwortlich: Dr. und Mönch Tam Thanh; Laien: Frau Luong, Frau Nguyet und Frau Bich  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 27.06, 17:00 Uhr – 29.06, 17:00 Uhr – Einführung in die THAI‑MASSAGE zur gesunden Pflege von Gelenken und Körper  

  – Verantwortlich: Frau Hoa und Frau Ha, erfahrende Therapeutinnen  

  – Bemerkung: Voranmeldung notwendig

• **Juli 2025**  

 ◦ 18.07, 17:00 Uhr – Vegan Kochkurs: „Kulinarische Kultur der vietnamesischen Mönche und Nonnen; Tagesablauf eines Mönches“  

  – Verantwortlich: Herr Do Xuân Ngoc und Frau Bich Nga, erfahrende Koch/Köchin (Laien)  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 25.07, 17:00 Uhr – 27.07, 17:00 Uhr – Gehmeditationskurs für alle Altersgruppen  

  – Verantwortlich: Lehrer und Mönch Phap Cu; Herr Dao Ngoc Hanh (Laie)  

  – Bemerkung: Voranmeldung notwendig

• **August 2025**  

 ◦ 03.08, 9:00 Uhr – 07.08, 17:00 Uhr – Sommer-Camp für Kinder und Jugendliche  

  – Verantwortlich: Lehrer und Mönch Phap Cu sowie der Vorstand  

  – Bemerkung: Bei Übernachtung – Voranmeldung notwendig  

 ◦ 17.08, 10:00 Uhr –23.08, 15:00 Uhr – Unterbringung und Versorgung der Gäste zur Konferenz in Dresden  

  – Verantwortlich: Kerstin & Wilfried  und Verwaltung  

  – Bemerkung: Angemeldet  

 ◦ 29.08, 17:00 Uhr – 31.08, 17:00 Uhr – Pflege und Heilung von Krankheiten nach fernöstlicher traditioneller Medizin oder TCM (Akupressur/Akupunktur)  

  – Verantwortlich: Frau Minh Nguyet, Dipl. Ing. Päd. TCM‑Heilpraktikerin  

  – Bemerkung: Voranmeldung notwendig

• **September 2025**  

 ◦ 07.09, 09:00 Uhr und 18:00 Uhr – Ahnendankfest (Ullambana): Tag der offenen Tür am Institut KHANH AN für Meditation  

  – Verantwortlich: Vorstand  

  – Bemerkung: Eintritt frei, ohne Anmeldung, unbegrenzte Teilnehmerzahl  

 ◦ 12.09, 17:00 Uhr –14.09, 17:00 Uhr – JOGA-Kurs zur Stärkung der Gesundheit  

  – Verantwortlich: Lehrer …  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 19.09, 17:00 Uhr – 21.09, 17:00 Uhr – Praxis der Gehmeditation für alle  

  – Verantwortlich: Lehrer und Mönch Phap Cu  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 28.09, 14:00 Uhr – Dorffest in ZWIESEL  

  – Verantwortlich: Vertreter des Dorfes und Vorstand  

  – Bemerkung: Eintritt frei, unbegrenzte Teilnehmerzahl

• **Oktober 2025**  

 ◦ 03.10, 17:00 Uhr – 05.10, 17:00 Uhr – Zweitäige Praxis zum Herbstfest: „8. Vollmondfest – ein Retreat für Familien“  

  – Verantwortlich: Lehrer und Mönch Phap Cu  

  – Bemerkung: Unbegrenzte Besucherzahl; bei Übernachtung: Voranmeldung notwendig  

 ◦ 10.10, 17:00 Uhr – 12.10, 17:00 Uhr – Fußmassage: „Eine wichtige Gesundheitspflege“  

  – Verantwortlich: Frau Hoa und Frau Ha, erfahrende Therapeutinnen  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 26.10, 9:00–17:00 Uhr – TAG DER ACHSAMKEIT FÜR ALLE  

  – Verantwortlich: Lehrer und Mönch Phap Chuong (aus Zen‑Waldtempel)  

  – Bemerkung: Unbegrenzte Besucherzahl

• **November 2025**  

 ◦ 31.10, 17:00 Uhr – 02.11, 17:00 Uhr – Intensiver TAI CHI-Kurs (16 Stellungen)  

  – Verantwortlich: Lehrer und Mönch Phap Cu  

  – Bemerkung: Voranmeldung notwendig  

 ◦ 14.11, 17:00 Uhr –16.11, 17:00 Uhr – Shaolin Qi Gong-Kurs  

  – Verantwortlich: Lehrer und Mönch Phap Cu  

  – Bemerkung: Voranmeldung notwendig
