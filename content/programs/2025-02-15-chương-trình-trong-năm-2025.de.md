---
title: KURSE FÜR 2025
meta_title: KURSE FÜR 2025
description: KURSE FÜR 2025
---
### 04.04 – 05.04 MIT FREUDE UND GENUSS VEGAN KOCHEN

Ein Kurs mit Herrn Do Xuan Ngoc und Frau Bich Nga

Vegane Ernährung ist keine Form der Askese, sondern unterstützt unsere Gesundheit und schenkt uns vielfältige Genüsse. Sie hilft zugleich, unser Mitgefühl mit den Tieren zu kultivieren. In diesem Kurs steht die Freude am gemeinsamen achtsamen Kochen und Genießen im Mittelpunkt des Geschehens. Wir bereiten Gerichte aus der asiatischen Küche zu, die Sie zu Hause einfach nachkochen können. Freuen wir uns auf die Entdeckung exotischer

kulinarischer Genüsse aus der veganen Küche Asiens!

Kurssprache: Deutsch/Vietnamesisch

### 18.04.–21.04. GROSSE RETREATS mit den Mönchen des Instituts KHANH AN für Meditation Oster-Retreat 

Ein Kurs mit dem Lehrer und Mönch Phap Cu 

Ein Retreat mit den Mönchen und Nonnen des Instituts KHANH AN für Meditation. Eine Zeit für Alleinstehende, Familien und Freunde, um uns gemeinsam in Achtsamkeit zu üben und den Frieden in uns selbst und in unseren Familien zu stärken. Neben den täglichen Meditationen, Dharma-Vorträgen und dem Austausch in Gruppen werden wir Gelegenheit finden, unsere Dankbarkeit und Freude auszudrücken. Ein Achtsamkeitsprogramm für Kinder (6–12 Jahre) und Teenager (13–17 Jahre) wird angeboten. 

### Retreat-Sprache für Erwachsene und Teenager: Deutsch, Englisch, Vietnamesisch Sprache im Kinderprogramm: Deutsch, Englisch, Vietnamesisch\

02-04.05 Eine geführte Meditation, um Stabilität zu kultivieren und Mitgefühl auszustrahlen 

Ein Kurs mit dem Mönch und Lehrer Phap Cu

In dieser für die Welt schmerzlichen Zeit bieten wir eine Meditation an, die helfen soll, Stabilität und Festigkeit zu kultivieren, damit wir unsere Herzen öffnen, Mitgefühl ausstrahlen und alle unterstützen können, die unter Krieg und anderen Katastrophen leiden. Die große Energie des Mitgefühls kann Wut, Hass und Gewalt abkühlen.

Retreatsprache: Deutsch und Vietnamesisch

### 30.05-01.06 FRIEDEN IN MIR - FRIEDEN IN DER WELT: Schritt-für-Schritt Einführung in die Praxis der Achtsamkeit und Meditation / PEACE IN ME - PEACE IN THE WORLD: Step-by-step introduction to the practice of mindfulness and meditation 

Gehmeditation mit den Mönch und Lehrer Phap Chuong

Die Gehmeditation ist eine Übung in Achtsamkeit und Konzentration. Unser Geist bleibt auf unsere Füße und auf unser Gehen gerichtet. Dies bringt uns weg von unserem ständigen Denken und Fühlen, von unseren Sorgen, unserem Ärger und unserer inneren Unruhe. Wir gehen, um zu gehen, nicht um anzukommen – völlig entspannt, mit leichten Schritten. Wir werden dabei ruhig und stabil. Längere Gehmeditationen in der freien Natur gehören zu den schönsten Praxisformen. Sie bieten uns nicht nur die Möglichkeit, meditative Tiefe zu erleben, sondern helfen uns auch, unsere Herzen für die Wunder der Schöpfung zu öffnen und deren heilsame und nährende Einflüsse anzunehmen.

Retreatsprache: Deutsch und Vietnamesisch 

### 07.–09.06 TAI CHI 8 FORMEN FÜR ANFÄNGER

Ein Kurs mit dem Mönch und Lehrer Phap Cu

In diesem Kurs erlernen wir die Kunst der natürlichen Bewegungen in einer festgelegten Abfolge weicher und fließender Übungen, die dem menschlichen Körper entsprechen. Bei diesen Bewegungen befinden sich Yin und Yang in vollkommener Harmonie. Dadurch werden Körper und Geist von Stress und Verspannungen befreit, was die Minderung von Rückenschmerzen und Haltungsschäden fördert. Die Tai-Chi-Übungen werden umrahmt von täglicher Meditation und Tiefenentspannung. Dies stabilisiert unsere psychische und körperliche Gesundheit und führt zu mehr Ausgeglichenheit und Lebensfreude.

Kurssprache: Englisch/Deutsch

### 13 – 17.06 Fünftätiger Kurs zur Reinigung des Körpers und der Seele

Der Erneuerungsprozess – Reinigung von Körper und Geist

Dr. Dr. und Mönch Thich Tâm Thanh

Geboren 1984 in Quy Nhon-Binh Dinh Vietnam. Im Jahr 1987 schloss er sein Medizinstudium an der Universität Saigon ab und unterrichtete dort bis 1994 als Fachdozent für Labordiagnostik. Im Jahr 2001 promovierte er und schloss sein Studium der Pharmazie mit Schwerpunkt Diabetes und Ernährung in den USA ab.

Der Erneuerungsprozess – die Reinigung von Körper und Geist – ist ein Vorgang der Entgiftung, bei dem angesammelte Rückstände im Körper und Geist ausgeschieden werden. Gleichzeitig werden wertvolle Nährstoffe aus grober und feiner Nahrung aufgenommen, ebenso wie die richtigen Erkenntnisse, um eine angemessene Pflege und Heilung zu ermöglichen.

Der Prozess der Reinigung und Transformation von Körper und Geist hilft dir, eine umfassende Erneuerung durchzuführen. Die Entgiftung erfolgt parallel zur therapeutischen Nährstoffaufnahme, Entspannung, inneren Erleuchtung, Therapie, Bewegung und der praktischen Anwendung der Ölspülung. Alle diese Methoden zielen darauf ab, ein einziges Ziel zu erreichen: Einen gesunden Körper – einen ruhigen Geist – einen klaren Verstand.

Retreatsprache: Deutsch - Englisch

### 17 -29.06 Einführung in die THAI- MASSAGE 

Ein Kurs mit Frau Hoa 

zur gesunden Pflege der Gelenke und des ganzen Körpers

Die Thai - Massage basiert auf der Arbeit entlang bestimmter Energie-Linien im Körper, ähnlich den Meridianen in der traditionellen chinesischen Medizin. Die Massage darauf ab, energetische Blockaden in diesen Linien zu lösen und den Fluss der Energie zu harmonisieren.

Kurssprache: Deutsch und Vietnamesisch

### 29 - 31.08 Pflege und Heilung von Krankheiten nach fernöstlicher traditioneller Medizin oder TCM – Akupressur und Akupunktur

Frau Minh Nguyet, Dipl. Ing. Päd., Heilpraktikerin

Retreatsprache: Deutsch 

### 19.10 TAG DER ACHTSAMKEIT - Innere Ruhe finden in einer lauten Welt 

Zeit: 10 – 17:00 Uhr

 Eine Zeit für Alleinstehende, Familien und Freunde, um uns gemeinsam in Achtsamkeit zu üben und den Frieden in uns selbst und in unseren Familien zu stärken. Neben den täglichen Meditationen, Dharma-Vorträgen und dem Austausch in Gruppen werden wir Gelegenheit finden, unsere Dankbarkeit und Freude auszudrücken. Retreatsprache: Deutsch und Vietnamesisch Ein intensiver TAI CHI-Kurs, 16 Stellungen

### 31.10 – 02.11 Ein intensiver TAI CHI-Kurs, 16 FORMEN

Ein Kurs mit dem Mönch und Lehrer Phap Cu

In diesem Kurs erlernen wir die Kunst der natürlichen Bewegungen in einer festgelegten Abfolge weicher und fließender Übungen, die dem menschlichen Körper entsprechen. Bei diesen Bewegungen befinden sich Yin und Yang in vollkommener Harmonie. Dadurch werden Körper und Geist von Stress und Verspannungen befreit, was die Minderung von Rückenschmerzen und Haltungsschäden fördert. Die Tai-Chi-Übungen werden umrahmt von täglicher Meditation und Tiefenentspannung. Dies stabilisiert unsere psychische und körperliche Gesundheit und führt zu mehr Ausgeglichenheit und Lebensfreude.

Kurssprache: Englisch/Deutsch

### 14. – 16.11 Shaolin Qi Gong -Kurs 	

Ein Kurs mit dem Mönch und Lehrer Phap Cu

In unserem Alltagsleben brauchen wir Ruhe, Stabilität und Gelassenheit, um in unerwarteten Situationen oder bei Stress nicht aus der Bahn geworfen zu werden. Um das zu erreichen,

müssen wir eine gute Gesundheit im Geist und im Körper und eine positive Energie besitzen. Die Meditation hilft uns dabei, einen klaren und gesunden Geist zu haben. Durch die bewusste

Atmung und die achtsame Bewegung des Körpers beim Qi Gong kann die positive Energie im Körper aktiviert werden. Blockaden im Körper und im Geist werden hierdurch gelöst. Die Praxis der Meditation und des Qi Gong ist eine gesunde Lebensführung, die wir in diesem Kurs lernen, um sie im Alltag einzusetzen.

Kurssprache: Englisch/Deutsch

### 27.12.25–02.01.26 NEUJAHRS-RETREAT 

Mit den Mönchen und Nonnen aus vielen Ländern

Das Leben Feiern Ferien - Retreat für alle mit den Mönchen und Nonnen des Instituts KHANH AN für Meditation. Dieses Retreat ist für alle: Für Erwachsene, Teenager und Kinder. Es wird ein Programm für Kinder und Jugendliche angeboten (6–12 und 13–16 Jahre). Wir werden gemeinsam die Ferien in einer spirituellen Weise verbringen, die allen Freude bereitet. Wir werden singen, Darbietungen gestalten, Sitz-, Geh- und Ess-Meditation praktizieren und unser Leben in der Tiefe betrachten, um zu erkennen, wie wir wirklich glücklich zusammenleben können. Es wird auch eine Zeit der Besinnung geben, um dem neuen Jahr in Kontemplation entgegenzusehen und das Jahr 2026 bewusst zu beginnen. Kurssprachen: Deutsch und Englisch; Sprachen im Kinderprogramm: Deutsch und Vietnamesisch
