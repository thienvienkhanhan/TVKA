---
title: Chương trình trong năm 2025
meta_title: Chương trình trong năm 2025
description: Chương trình trong năm 2025
---
### 04.04 – 05.04 NẤU ĂN VEGAN VỚI NIỀM VUI VÀ THƯƠNG THỨC  

Một khóa học với ông Do Xuan Ngoc và bà Bich Nga  

Chế độ ăn thuần chay không phải là hình thức khổ hạnh, mà hỗ trợ sức khỏe của chúng ta và mang lại nhiều niềm vui đa dạng. Nó cũng giúp chúng ta nuôi dưỡng lòng từ bi đối với các loài vật. Trong khóa học này, niềm vui khi cùng nhau nấu ăn và thưởng thức một cách chánh niệm được đặt lên hàng đầu. Chúng ta sẽ cùng nhau chế biến các món ăn thuộc ẩm thực châu Á, mà bạn có thể dễ dàng tự nấu tại nhà. Hãy cùng nhau khám phá những hương vị ẩm thực độc đáo từ ẩm thực thuần chay châu Á!  

Ngôn ngữ khóa học: Tiếng Đức/Tiếng Việt

### 18.04.–21.04 RETREAT LỚN CÙNG CÁC TĂNG – NI TỪ VIỆN KHANH AN CHO THIỀN (nhân ngày lễ Phục Sinh)  

Một khóa học với giáo viên và tăng Phap Cu  

Đây là một retreat cùng các tăng và ni của Viện KHANH AN cho thiền. Thời gian này dành cho người độc thân, gia đình và bạn bè, để cùng nhau thực hành chánh niệm và củng cố sự bình an trong chính mình cũng như trong gia đình. Bên cạnh các bài thiền hàng ngày, các bài giảng về Pháp và những buổi trao đổi nhóm, chúng ta còn có cơ hội thể hiện lòng biết ơn và niềm vui. Chương trình chánh niệm cho trẻ em (6–12 tuổi) và thanh thiếu niên (13–17 tuổi) cũng được cung cấp.  

Ngôn ngữ retreat cho người lớn và thanh thiếu niên: Tiếng Đức, Tiếng Anh, Tiếng Việt; Ngôn ngữ trong chương trình trẻ em: Tiếng Đức, Tiếng Anh, Tiếng Việt

### 02-04.05 BÀI THIỀN CÓ HƯỚNG DẪN – NUÔI DƯỠNG SỰ ỔN ĐỊNH VÀ LAN TỎA LÒNG TỪ BI  

Một khóa học với tăng và giáo viên Phap Cu  

Trong thời điểm đau thương của thế giới này, chúng ta tổ chức một bài thiền nhằm giúp nuôi dưỡng sự ổn định và vững chắc, để có thể mở rộng trái tim, lan tỏa lòng từ bi và hỗ trợ tất cả những ai đang chịu đựng do chiến tranh và các thảm họa khác. Năng lượng lớn của lòng từ bi có thể làm dịu đi cơn giận, hận và bạo lực.  

Ngôn ngữ: Tiếng Đức và Tiếng Việt

### 30.05-01.06 HÒA BÌNH TRONG TÔI – HÒA BÌNH TRÊN THẾ GIỚI: HƯỚNG DẪN TỪNG BƯỚC VÀO THỰC HÀNH CHÁNH NIỆM VÀ THIỀN  

(PEACE IN ME – PEACE IN THE WORLD: Step-by-step introduction to the practice of mindfulness and meditation)  

Thiền đi bộ cùng tăng và giáo viên Phap Chuong  

Thiền đi bộ là bài tập về chánh niệm và tập trung, giúp tâm trí chúng ta hướng về đôi chân và từng bước đi. Điều này giúp chúng ta rời xa những suy nghĩ và cảm xúc liên tục, khỏi những lo âu, giận dữ và sự bất an nội tâm. Chúng ta đi không phải để đến đích mà chỉ đơn giản là đi – hoàn toàn thư giãn, với những bước đi nhẹ nhàng. Qua đó, chúng ta trở nên bình tĩnh và ổn định. Những buổi thiền đi bộ dài ngoài thiên nhiên không chỉ mang lại chiều sâu thiền định mà còn giúp mở rộng trái tim đón nhận những điều kỳ diệu của tạo hóa cùng những ảnh hưởng chữa lành và nuôi dưỡng từ đó.  

Ngôn ngữ: Tiếng Đức và Tiếng Việt

### 07.–09.06 TAI CHI 8 HÌNH THỨC CHO NGƯỜI MỚI BẮT ĐẦU  

Một khóa học với tăng và giáo viên Phap Cu  

Trong khóa học này, chúng ta sẽ học nghệ thuật của những chuyển động tự nhiên trong một chuỗi bài tập mềm mại và linh hoạt, phù hợp với cơ thể con người. Các chuyển động này hòa quyện hoàn hảo giữa âm và dương, giúp cơ thể và tâm trí được giải phóng khỏi căng thẳng và mệt mỏi, góp phần giảm đau lưng và cải thiện tư thế. Các bài tập Tai Chi được kết hợp với thiền hàng ngày và thư giãn sâu, giúp ổn định sức khỏe tinh thần cũng như thể chất, mang lại sự cân bằng và niềm vui trong cuộc sống.  

Ngôn ngữ khóa học: Tiếng Anh/Tiếng Đức

### 13 – 17.06 KHÓA HỌC NĂM NGÀY VỀ THANH LỌC CƠ THỂ VÀ TÂM HỒN  

“Quá trình đổi mới – Thanh lọc cơ thể và tâm trí”  

Dr. Dr. và tăng Thích Tâm Thanh  

Sinh năm 1984 tại Quy Nhơn – Bình Định, Việt Nam. Năm 1987, ông tốt nghiệp ngành Y tại Đại học Sài Gòn và giảng dạy tại đây cho đến năm 1994 với vai trò giảng viên chuyên ngành chẩn đoán lâm sàng. Năm 2001, ông hoàn thành tiến sĩ và tốt nghiệp ngành Dược với chuyên ngành tiểu đường và dinh dưỡng tại Hoa Kỳ.  

Quá trình đổi mới – thanh lọc cơ thể và tâm trí – là quá trình giải độc, loại bỏ các dư tích đã tích tụ trong cơ thể và tâm trí, đồng thời hấp thụ các dưỡng chất quý giá từ thực phẩm thô và tinh khiết, cùng với những hiểu biết đúng đắn nhằm đảm bảo việc chăm sóc và chữa lành thích hợp.  

Quá trình thanh lọc và chuyển hóa cơ thể và tâm trí giúp bạn thực hiện một sự đổi mới toàn diện. Quá trình giải độc được thực hiện song song với việc hấp thu dưỡng chất có tính điều trị, thư giãn, giác ngộ nội tâm, liệu pháp, vận động và áp dụng thực hành súc dầu. Tất cả các phương pháp này nhằm đạt được một mục tiêu duy nhất: Một cơ thể khỏe mạnh – một tâm trí bình an – một trí óc minh mẫn.  

Ngôn ngữ : Tiếng Đức – Tiếng Anh

### 17 – 29.06 GIỚI THIỆU VỀ THAI MASSAGE  

Một khóa học với bà Hoa  

Về chăm sóc sức khỏe các khớp và toàn bộ cơ thể  

Thai Massage dựa trên việc làm việc dọc theo các đường năng lượng nhất định trong cơ thể, tương tự như các kinh mạch trong y học cổ truyền Trung Hoa. Phương pháp massage này nhằm giải phóng các tắc nghẽn năng lượng trên các đường đó và điều hòa luồng năng lượng.  

Ngôn ngữ khóa học: Tiếng Đức và Tiếng Việt

### 29 – 31.08 CHĂM SÓC VÀ CHỮA LÀNH BỆNH TẬT THEO Y HỌC TRUYỀN THỐNG PHƯƠNG ĐÔNG (TCM) – CHÂM CỨU VÀ BẤM HUYỆT  

Bà Minh Nguyet, Kỹ sư có văn bằng, nhà thực hành chữa bệnh tự nhiên  

Ngôn ngữ: Tiếng Đức

### 19.10 NGÀY CHÁNH NIỆM – TÌM KIẾM BÌNH AN NỘI TẠI TRONG MỘT THẾ GIỚI ỒN ÀO  

Thời gian: 10 – 17:00  

Một khoảng thời gian dành cho người độc thân, gia đình và bạn bè, để cùng nhau thực hành chánh niệm và củng cố sự bình an trong bản thân và gia đình. Bên cạnh các bài thiền hàng ngày, các bài giảng về Pháp và những buổi trao đổi nhóm, chúng ta sẽ có cơ hội thể hiện lòng biết ơn và niềm vui.  

Ngôn ngữ: Tiếng Đức và Tiếng Việt  

Ngoài ra, còn có một khóa học Tai Chi chuyên sâu với 16 tư thế.

### 31.10 – 02.11 KHÓA HỌC TAI CHI CHUYÊN SÂU, 16 HÌNH THỨC  

Một khóa học với tăng và giáo viên Phap Cu  

Trong khóa học này, chúng ta sẽ học nghệ thuật của những chuyển động tự nhiên theo một chuỗi bài tập mềm mại và linh hoạt, phù hợp với cơ thể con người. Những chuyển động này hòa quyện hoàn hảo giữa âm và dương, giúp cơ thể và tâm trí được giải phóng khỏi căng thẳng và mệt mỏi, góp phần giảm đau lưng và cải thiện tư thế. Các bài tập Tai Chi được kết hợp với thiền hàng ngày và thư giãn sâu, giúp ổn định sức khỏe tinh thần và thể chất, mang lại sự cân bằng và niềm vui trong cuộc sống.  

Ngôn ngữ khóa học: Tiếng Anh/Tiếng Đức

### 14 – 16.11 KHÓA HỌC SHAOLIN QI GONG  

Một khóa học với tăng và giáo viên Phap Cu  

Trong cuộc sống hằng ngày, chúng ta cần sự yên tĩnh, ổn định và điềm đạm để không bị xao nhãng trong những tình huống bất ngờ hoặc khi căng thẳng. Để đạt được điều đó, chúng ta cần có một sức khỏe tinh thần và thể chất tốt cùng với năng lượng tích cực. Thiền giúp chúng ta có được một tâm trí rõ ràng và khỏe mạnh. Thông qua việc thở có ý thức và chuyển động chánh niệm khi thực hành Qi Gong, năng lượng tích cực trong cơ thể được kích hoạt, giúp giải phóng các tắc nghẽn trong cơ thể và tâm trí. Thực hành thiền và Qi Gong là một lối sống lành mạnh mà chúng ta học được trong khóa học này để áp dụng vào cuộc sống hàng ngày.  

Ngôn ngữ khóa học: Tiếng Anh/Tiếng Đức

### 27.12.25–02.01.26 RETREAT TẾT  

Cùng với các Tăng và Ni đến từ nhiều quốc gia  

“Cuộc sống là một lễ hội” – Retreat dành cho tất cả mọi người cùng các Tăng và Ni của Viện KHANH AN cho Thiền. Retreat này dành cho tất cả: người lớn, thanh thiếu niên và trẻ em. Chương trình cho trẻ em và thanh thiếu niên (6–12 và 13–16 tuổi) cũng được tổ chức. Chúng ta sẽ cùng nhau trải qua kỳ nghỉ theo một cách thức tâm linh, mang lại niềm vui cho mọi người. Chúng ta sẽ hát, tổ chức các tiết mục biểu diễn, thực hành thiền ngồi, thiền đi bộ và thiền khi ăn, cũng như cùng nhau suy ngẫm sâu sắc về cuộc sống để nhận ra cách sống hạnh phúc thực sự. Cũng sẽ có thời gian để suy tư, để đón chào năm mới với tâm trạng tỉnh táo và bắt đầu năm 2026 một cách có ý thức.  

Ngôn ngữ khóa học: Tiếng Đức và Tiếng Anh; Ngôn ngữ trong chương trình trẻ em: Tiếng Đức và Tiếng Việt
